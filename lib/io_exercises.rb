# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  goal = rand(100) + 1
  result = false
  num_guessses = 0
  puts "Hi, let's play a game! Try to guess a number between 1 and 100. Type it in: "
  until result == true
    input = gets.chomp
    puts "_____________________________"
    print input
    if input.to_i < goal
      puts " is too low\n-------------"

    elsif input.to_i > goal
      puts " is too high\n-------------"
    else
      result = true
    end
    num_guessses += 1
  end
  puts "                      "
  puts "It took you #{num_guessses} attempts to guess\nYou won!!!"
end

if __FILE__ == $PROGRAM_NAME
  puts "Please provide a filename!"
  file1 = gets.chomp
  file2 = File.open("#{file1}-shuffled", 'w')
  File.open(file1).each do |line|
    shuffled = line.chars.shuffle.join
    file2.write(shuffled)
  end
  file2.close
end
